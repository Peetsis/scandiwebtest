const cdField = document.querySelector(".input-type-cd");
const furnitureField = document.querySelector(".input-type-furniture");
const bookField = document.querySelector(".input-type-book");
const allFields = document.querySelector(".types")

let typeMenu = document.querySelector('select');

furnitureField.remove();
bookField.remove();

function reveal(newField) {
    allFields.innerHTML = "";
    allFields.appendChild(newField);
}

typeMenu.addEventListener('change', () => {
    var typeField = typeMenu.options[typeMenu.selectedIndex].value
    switch(typeField){
        case "disk": 
            return reveal(cdField);
        case "book":
            return reveal(bookField);
        case "furniture":
            return reveal(furnitureField); 
    }
}); 
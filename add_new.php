<?php

require_once 'includes/product.class.php';
require_once 'includes/book.class.php';
require_once 'includes/cd.class.php';
require_once 'includes/furniture.class.php';
require_once 'includes/database.php';

$database = new Database();
$db = $database->connect();

$book = new Book($db);
$furniture = new Furniture($db);
$cd = new Cd($db);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Product</title>
    <link rel="stylesheet" href="stylesheets/style.css">
</head>
<body>
    <div class="header">
        <h1> Product Add </h1>
        <div class="buttons">
            <a class="add" type="button" href="index.php">Product List</a>
        </div>
    </div>
    <hr>

    <?php 
        $products = array(
            "disk" => $cd,
            "book" => $book,
            "furniture" => $furniture
        );
        
        $typeSwitcher = $_POST['type_switcher'] ?? '';
        $selectedProduct = $products[$typeSwitcher] ?? '';

        if($typeSwitcher){
           $selectedProduct->post_product();
        }
    ?>

    <div class="add-form">
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                <div class="input-fields">
                    <label for="sku">SKU</label>
                    <input type="text" name="sku" id="sku" required>
                    <br><br>
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" required>
                    <br><br>
                    <label for="price">Price</label>
                    <input type="number" name="price" id="price" required>
                    <br><br>
                </div>

                <label for="type_switcher">Type switcher</label>
                <select name="type_switcher" id="type_switcher">
                    <option value="disk">CD Disc</option>
                    <option value="book">Book</option>
                    <option value="furniture">Furniture</option>
                </select>

                <div class="types">
                    <div class="input-type-cd">
                        <label for="size">Size</label>
                        <input type="number" name="size" id="size" required>
                        <br><br>
                        <p>CD disc space in MB</p>
                    </div>

                    <div class="input-type-furniture">
                        <label for="height">Height</label>
                        <input type="number" name="height" id="height" required>
                        <br><br>
                        <label for="width">Width</label>
                        <input type="number" name="width" id="width" required>
                        <br><br>
                        <label for="length">Length</label>
                        <input type="number" name="length" id="length" required>
                        <br><br>
                        <p>Furniture dimensions in HxWxL format</p>
                    </div>

                    <div class="input-type-book">
                        <label for="weight">Weight</label>
                        <input type="number" name="weight" id="weight" required>
                        <br><br>
                        <p>Weight of the book in kg</p>
                    </div>
                </div>
                <button class="save" type="submit" name="submit">Save</button>
            </form>
    </div>    
    <script src="script.js"></script>
</body>
</html>
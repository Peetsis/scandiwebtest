<?php

class Database 
{
    private $host = 'localhost';
    private $name = 'scandiwebtest';
    private $username = 'root';
    private $password = '';
    public $connection;

    public function connect()
    {
        try{
            $this->connection = new PDO('mysql:server=' . $this->host . ";dbname=" . $this->name, $this->username, $this->password
            );
        } catch(PDOException $e) {
            echo "DB Connect error: " . $e->getMessage();
        }
        return $this->connection;
    }
}


<?php

class Furniture extends Product 
{

    public $height;
    public $width;
    public $length; 

    public function type() 
    {
        return "Dimension: " . 
        $this->height . "x" . 
        $this->width . "x" . 
        $this->length;
    }

    public function get_params()
    {
        $params = array(
            $this->sku = $_POST['sku'],
            $this->name = $_POST['name'],
            $this->price = $_POST['price'],
            $this->height = $_POST['height'],
            $this->width = $_POST['width'],
            $this->length = $_POST['length']
        );
        return $params;
    }
}
<?php

class Book extends Product 
{
    public $weight;
    
    public function type() 
    {
        return "Weight: " . $this->weight . " kg";
    }

    public function get_params()
    {
        $params = array(
            $this->sku = $_POST['sku'],
            $this->name = $_POST['name'],
            $this->price = $_POST['price'],
            $this->weight = $_POST['weight']
        );
        return $params;
    }
}
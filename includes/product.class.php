<?php

class Product 
{
    public $id;
    public $sku;
    public $name;
    public $price;
    public $type;
    private $connection;
    private $table_name = "product";

    public function __construct($db)
    {
        $this->connection = $db;
    }

    public function type() 
    {
        return $this->type;
    }

    public function add_product($product_type)
    {

        $query = "INSERT INTO " . $this->table_name . " SET sku=:sku, name=:name, price=:price, type=:type";
  
        $stmt = $this->connection->prepare($query);

        $this->sku=htmlspecialchars(strip_tags($this->sku));
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->price=htmlspecialchars(strip_tags($this->price));
        $this->type=htmlspecialchars(strip_tags($this->type()));

        $stmt->bindParam(":sku", $this->sku);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":price", $this->price);
        $stmt->bindParam(":type", $product_type);

        if($stmt->execute()){
            return true;
        } else {
            return false;
        }
    }

    public function get_params()
    {
        $params = array(
            $this->sku = $_POST['sku'],
            $this->name = $_POST['name'],
            $this->price = $_POST['price']
        );
        return $params;
    }

    public function post_product()
    {
        $this->get_params();
        if($this->add_product($this->type())){
            echo "<div class='add-form'>";
            echo " '{$this->name}' added to products!";
            echo "</div>";
        } else {
            echo "Failed!";
        }
    }

    public function get_products()
    {
        $query = "SELECT * FROM " . $this->table_name;
        $stmt = $this->connection->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    public function display_products()
    {
        $stmt = $this->get_products();
        $num = $stmt->rowCount();

        if($num > 0)
            {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
                { 
                    extract($row); ?>
                    <div class='product'>
                        <div class='check'>
                        <input type='checkbox' name='checkbox[]' value='<?php echo $row['id'] ?>'>
                        </div>

                        <div class='sku'>
                            <h3> SKU: <?php echo $sku ?> </h3>
                        </div>

                        <div class='name'>
                            <h3> Name: <?php echo $name ?> </h3>
                        </div>

                        <div class='price'>
                            <h3> Price: <?php echo $price ?> $ </h3>
                        </div>

                        <div class='special_attribute'>
                            <h3> <?php echo $type ?> </h3>
                        </div>
                    </div>
                <?php }
            } else { ?>
                <div class='products'>
                    <h1> Product list is empty </h1>
                </div>
            <?php }
    }

    public function delete_products($deleteList)
    {
        $query = "DELETE FROM " . $this->table_name . " WHERE id IN " . "(" . $deleteList . ")";
        $stmt = $this->connection->prepare($query);
        $stmt->execute();

        if($stmt->execute()){
            echo "<meta http-equiv='refresh' content='0'>";
        } else {
            echo "Fail..";
        }
    }
}
<?php

class Cd extends Product 
{
    public $size;

    public function type() 
    {
        return "Size: " . $this->size . " MB";
    }

    public function get_params()
    {
        $params = array(
            $this->sku = $_POST['sku'],
            $this->name = $_POST['name'],
            $this->price = $_POST['price'],
            $this->size = $_POST['size']
        );
        return $params;
    }
}
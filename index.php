<?php 

require_once 'includes/product.class.php';
require_once 'includes/database.php';

$database = new Database();
$db = $database->connect();
$products = new Product($db);
$stmt = $products->get_products();
$num = $stmt->rowCount();

$deleteCheckbox = $_POST['checkbox'] ?? '';
if($_POST)
{
    $deleteList = implode(", ", $deleteCheckbox);
    $products->delete_products($deleteList);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product List</title>
    <link rel="stylesheet" href="stylesheets/style.css">
</head>
<body>
    <form action="index.php" method="post">
    <div class="product_list">
        <div class="header">
            <h1> Product List </h1>
            <div class="buttons">
                <button class="delete" type="submit" value="delete">Mass Delete</button>
                <a class="add" type="button" href="add_new.php">Add Product</a>
            </div>
        </div>
        <hr>
        <div class="products">
            <?php $product = $products->display_products(); ?>
        </div>
    </div>
    </form>
</body>
</html>